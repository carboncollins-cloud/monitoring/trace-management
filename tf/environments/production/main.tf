terraform {
  backend "consul" {
    path = "terraform/monitoring-trace-management"
  }

  required_providers {
    nomad = {
      source = "hashicorp/nomad"
      version = "2.1.0"
    }
    consul = {
      source = "hashicorp/consul"
      version = "2.20.0"
    }
  }
}

provider "nomad" {}
provider "consul" {}

module "intentions" {
  source = "../../modules/tempoConsulIntentions"
}

module "soc_volumes" {
  source = "../../modules/tempoNomadVolumes"

  plugin_id = "soc-axion-smb"

  cifs_user_id = 3205
  cifs_group_id = 3205
}
