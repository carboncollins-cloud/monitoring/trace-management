# Trace Management

[[_TOC_]]

## Description

Trace management using [Tempo](https://grafana.com/oss/tempo/) to centralise traces between all services and servers running within the [CarbonCollins - Cloud](https://gitlab.com/carboncollins-cloud) project.

## Other Info

I am still evaluating this service so configurations and setup may be subject to change
